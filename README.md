## Pipeline status
- master: [![pipeline status](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/master/pipeline.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/master)
[![coverage report](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/master/coverage.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/master)
- alghi: [![pipeline status](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/alghi/pipeline.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/alghi) 
[![coverage report](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/alghi/coverage.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/alghi)
- rocky-dev: [![pipeline status](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/rocky-dev/pipeline.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/rocky-dev) 
[![coverage report](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/rocky-dev/coverage.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/rocky-dev)
- lia-fitur: [![pipeline status](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/lia-fitur/pipeline.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/lia-fitur) 
[![coverage report](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/lia-fitur/coverage.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/lia-fitur)
- inez: [![pipeline status](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/inez/pipeline.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/inez) 
[![coverage report](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/badges/inez/coverage.svg)](https://gitlab.com/firdaus.alghifari/advprog-teman-ai/-/commits/inez)

## Nama Kelompok:
- Firdaus Al-ghifari (1806133780)
- Rocky Arkan (1806186566)
- Lia Yuliana (1806141271)
- Inez Amandha Suci (1806141233)

## Deskripsi Aplikasi:
Teman.ai (Team Management Artificial Intelligence) adalah sebuah ChatBot berbasis aplikasi Line yang bertujuan untuk memanage task dalam suatu team dengan menggunakan fitur to-do-list dari Trello.

## How to add Teman.ai
Scan QR Code ini di aplikasi Line anda

![](qr-teman-ai.png)
