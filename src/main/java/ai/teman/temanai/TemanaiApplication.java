package ai.teman.temanai;

import ai.teman.temanai.service.TemanaiService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import java.util.logging.Logger;

@SpringBootApplication
@LineMessageHandler
public class TemanaiApplication extends SpringBootServletInitializer {

    private static final Logger MY_LOGGER = Logger.getLogger(TemanaiApplication.class.getName());
    private TemanaiService temanaiService;

    @Autowired
    public TemanaiApplication(TemanaiService temanaiService) {
        this.temanaiService = temanaiService;
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(TemanaiApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(TemanaiApplication.class, args);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent) {
        try {
            String message = messageEvent.getMessage().getText();
            Source chatSource = messageEvent.getSource();
            String replyToken = messageEvent.getReplyToken();
            temanaiService.replyMessage(replyToken, message, chatSource);
        } catch (Exception e) {
            MY_LOGGER.warning(e.getMessage());
        }
    }

}
