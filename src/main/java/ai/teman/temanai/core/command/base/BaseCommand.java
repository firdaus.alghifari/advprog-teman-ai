package ai.teman.temanai.core.command.base;

public interface BaseCommand {
    String getName();
    String getDetail();
}
