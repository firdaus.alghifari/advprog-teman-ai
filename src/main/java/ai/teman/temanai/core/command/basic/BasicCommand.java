package ai.teman.temanai.core.command.basic;

import ai.teman.temanai.core.command.base.BaseCommand;

public interface BasicCommand extends BaseCommand {
    String processCommand(String message);
}
