package ai.teman.temanai.core.command.basic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FunFactBasicCommand implements BasicCommand {

    private ArrayList<String> funFactList;

    public FunFactBasicCommand() {
        funFactList = new ArrayList<>();
        seedFunFactList();
    }

    @Override
    public String getName() {
        return "/funFact";
    }

    @Override
    public String processCommand(String message) {
        return getFunFact();
    }

    private void seedFunFactList() {
        funFactList.add("Teman.ai adalah akronim dari Team Management AI");
        funFactList.add("Yang ngebuat fitur ini ngantuk bro...");
        funFactList.add("Line Bot ini dibuat oleh Kelompok 6 Advanced Programming Kelas A 2020");
        funFactList.add("Bumi itu datar");
    }

    private String getFunFact() {
        return funFactList.get(new Random().nextInt(funFactList.size()));
    }

    public List<String> getFunFactList() {
        return Collections.unmodifiableList(funFactList);
    }

    @Override
    public String getDetail() {
        return "Sebuah command yang akan memberikan sebuah fun fact :D.\n"
                + "How to use:\n/funFact";
    }
}
