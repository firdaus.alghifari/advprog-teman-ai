package ai.teman.temanai.core.command.basic;

import com.linecorp.bot.model.message.FlexMessage;

public class HelpBasicCommand implements BasicFlexMessageCommand {

    @Override
    public String getName() {
        return "/help";
    }

    @Override
    public FlexMessage processCommand(String message) {
        return getHelp();
    }

    private static FlexMessage getHelp() {
        return new HelpFlexMessage().get();
    }

    private String getHelpString() {
        return "HELP\n\n" +
                "Register Trello Account\n" +
                "Register your Trello username to teman.ai\n" +
                "Make sure that you've already have Trello Account\n" +
                "How to use:\n/register username\n\n" +

                "Connect Board\n" +
                "Connect your Trello Board to teman.ai\n" +
                "How to use:\n/connectBoard BoardID\n" +
                "How to get your Board ID (PC Version):\n" +
                "1. Go to the board that you want to connect\n" +
                "2. Copy link of the board to new window\n" +
                "3. Edit and add .json in the of the link\n" +
                "4. Copy BoardID from {\"id\":\"BoardID\"}\n\n" +

                "Create List\n" +
                "Add a new Trello List to your Trello Board\n" +
                "How to use:\n/createList\nListName\n\n" +

                "Edit List\n" +
                "Rename a Trello List\n" +
                "How to use:\n/editList\nOldName\nNewName\n\n" +

                "Create Card\n" +
                "Add a new Trello Card to Trello List\n" +
                "How to use:\n/createCard\nListName\nCardName\nDueDate (optional)\n" +
                "DueDate should be in format of 'YYYY-MM-DD HH:MM'\n\n" +

                "Edit Card\n" +
                "Rename and/or edit due date of Trello Card\n" +
                "How to use:\n/editCard\nListName\nOldCardName\nNewCardName\nDueDate (optional)\n" +
                "DueDate should be in format of 'YYYY-MM-DD HH:MM'\n\n" +

                "Show Cards\n" +
                "View all Trello Card and Trello List in the board that you've connected\n" +
                "How to use:\n/showCards\n\n" +

                "Complete\n" +
                "Change task status in Trello Card that have due date to 'complete'\n" +
                "How to use:\n/complete\nListName\nCardName\n\n" +

                "Move Card\n" +
                "Move Card in List to another List\n" +
                "How to use:\n/moveCard\nListName\nCardName\nNewListName\n\n" +

                "Archive Card\n" +
                "Archive the card which is no longer needed\n" +
                "How to use:\n/archiveCard\nListName\nCardName\n\n" +

                "Daily Motivation\n" +
                "Motivation Quotes can show up on LINE Group\n" +
                "Daily Motivation have to set it first, otherwise Motivation Quotes won't show up" +
                "How to use:\n/setDailyMotivation number\n" +
                "Number means how many times motivation quotes will show up on your LINE Group in a day\n\n" +

                "Help\n" +
                "View list of teman.ai's commands\n" +
                "How to use:\n" +
                "/help\n\n" +

                "Fun Fact\n" +
                "Show a fun fact about anything\n" +
                "How to use:\n/funFact";
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan bantuan dalam menggunakan Bot Temanai.\n"
                + "How to use:\n/help";
    }

}
