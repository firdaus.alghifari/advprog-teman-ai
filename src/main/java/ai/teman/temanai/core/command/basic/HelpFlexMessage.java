package ai.teman.temanai.core.command.basic;

import static java.util.Arrays.asList;
import com.linecorp.bot.model.action.URIAction;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.component.Box;
import com.linecorp.bot.model.message.flex.component.Button;
import com.linecorp.bot.model.message.flex.component.Separator;
import com.linecorp.bot.model.message.flex.component.Text;
import com.linecorp.bot.model.message.flex.container.Bubble;
import com.linecorp.bot.model.message.flex.unit.FlexAlign;
import com.linecorp.bot.model.message.flex.unit.FlexFontSize;
import com.linecorp.bot.model.message.flex.unit.FlexLayout;
import com.linecorp.bot.model.message.flex.unit.FlexMarginSize;
import java.net.URI;
import java.util.function.Supplier;

public class HelpFlexMessage implements Supplier<FlexMessage> {

    @Override
    public FlexMessage get() {
        final Box body = createBody();
        final Box footer = createFooter();
        final Bubble bubble =
                Bubble.builder()
                        .body(body)
                        .footer(footer)
                        .build();
        return new FlexMessage("Help", bubble);
    }

    private Box createBody() {
        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .contents(asList(
                        Text.builder()
                                .text("LINEBOT")
                                .size(FlexFontSize.LG)
                                .flex(1)
                                .align(FlexAlign.START)
                                .build(),
                        Separator.builder()
                                .build(),
                        Text.builder()
                                .text("Hello World !")
                                .size(FlexFontSize.Md)
                                .flex(2)
                                .align(FlexAlign.END)
                                .build()
                ))
                .flex(1)
                .build();
    }

    private Box createFooter() {
        final Button dbutton =
                Button.builder()
                        .style(Button.ButtonStyle.PRIMARY)
                        .height(Button.ButtonHeight.SMALL)
                        .action(new URIAction("go to LINE",
                                URI.create("https://line.me/id/"), null))
                        .build();

        return Box.builder()
                .layout(FlexLayout.VERTICAL)
                .spacing(FlexMarginSize.SM)
                .contents(dbutton)
                .build();
    }
}