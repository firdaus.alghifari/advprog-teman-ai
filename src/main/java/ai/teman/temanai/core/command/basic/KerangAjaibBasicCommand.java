package ai.teman.temanai.core.command.basic;

import java.util.Random;

public class KerangAjaibBasicCommand implements BasicCommand {

    @Override
    public String getName() {
        return "/apakah";
    }

    @Override
    public String processCommand(String message) {
        int random = new Random().nextInt();
        return random % 2 == 1 ? "Ya" : "Engga";
    }

    @Override
    public String getDetail() {
        return "";
    }

}
