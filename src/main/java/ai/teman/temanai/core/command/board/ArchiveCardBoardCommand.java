package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.CardNotFoundException;
import ai.teman.temanai.core.trello.ListNotFoundException;
import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloCard;
import com.linecorp.bot.model.event.source.Source;

public class ArchiveCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/archiveCard";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length == 3) {
            String listName = messageLst[1];
            String cardName = messageLst[2];

            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, cardName);
                card.closed = true;
                TrelloApi.getInstance().updateCard(card.id, card);
                return "Card " + cardName + " is successfully archived";
            } catch (ListNotFoundException e) {
                return "List " + listName + " is not found";
            } catch (CardNotFoundException e) {
                return "Card " + cardName + " is not found";
            }
        } else {
            return "Invalid argument for /archiveCard. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "";
    }

}
