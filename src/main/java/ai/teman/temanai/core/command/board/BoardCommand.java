package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.command.base.BaseCommand;
import com.linecorp.bot.model.event.source.Source;

public interface BoardCommand extends BaseCommand {
    String processCommand(String message, Source chatSource, String boardId);
}
