package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.*;
import com.linecorp.bot.model.event.source.Source;

public class CompleteBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/complete";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageList = message.split("\n");
        if (messageList.length == 3) {
            String listName = messageList[1];
            String cardName = messageList[2];
            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, cardName);
                if (card.dueComplete == false) {
                    card.dueComplete = true;
                    TrelloApi.getInstance().updateCard(card.id, card);
                    return "Card " + cardName + " in " + listName + " is successfully completed";
                } else {
                    return "Card " + cardName + " in " + listName + " is already completed before";
                }
            } catch (ListNotFoundException e) {
                return "List " + listName + " is not found";
            } catch (CardNotFoundException e) {
                return "Card " + cardName + " is not found";
            }
        } else {
            return "Invalid argument for /complete. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "Change task status in Trello Card that have due date to 'complete'\n" +
                "How to use:\n/complete\nListName\nCardName";
    }
}
