package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloCard;
import ai.teman.temanai.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/createCard";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length > 4 || messageLst.length < 3) {
            return "Invalid argument for /createCard. See /help for details.";
        }

        Date due = null;
        String listName = messageLst[1];
        String cardName = messageLst[2];

        if (messageLst.length > 3) {
            String dateStr = messageLst[3];
            try {
                due = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateStr);
            } catch (ParseException e) {
                return String.format("'%s' is invalid date format. "
                        + "Please use: 'yyyy-MM-dd HH:mm'.%n", dateStr)
                        + "Example: 2020-12-31 23:59.";
            }
        }

        TrelloList[] lists = TrelloApi.getInstance().getBoardLists(boardId);

        for (TrelloList list : lists) {
            if (list.name.equals(listName)) {
                TrelloCard newCard = new TrelloCard();
                newCard.name = cardName;
                newCard.idBoard = boardId;
                newCard.idList = list.id;
                newCard.due = due;

                TrelloApi.getInstance().createCard(newCard);
                return String.format("Card '%s' is successfully created "
                        + "on list '%s' !", newCard.name, list.name);
            }
        }

        return String.format("List '%s' is not found on the connected board.", listName);
    }

    @Override
    public String getDetail() {
        return "Command untuk membuat card di suatu list.\n"
                + "How to use:\n/createCard\nnamaList\nnamaCard\n(optional) tanggalDue"
                + "tanggalDue harus berformat 'yyyy-MM-dd HH:mm'.\n"
                + "Contoh: 2020-12-31 23:59";
    }
}
