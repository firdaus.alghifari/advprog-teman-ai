package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;

public class CreateListBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/createList";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length <= 1) {
            return "Tidak ada List yang dibuat. Silakan coba lagi atau ketik "
                    + "\"/help\""
                    + " untuk meminta bantuan";
        } else {
            TrelloList newList = new TrelloList();
            newList.name = messageLst[1];
            newList.idBoard = boardId;
            TrelloApi.getInstance().createList(newList);
            return String.format("List '%s' berhasil dibuat", newList.name);
        }
    }

    @Override
    public String getDetail() {
        return "Command untuk membuat List.\n"
                + "How to use:\n/createList\nnamaList";
    }

}
