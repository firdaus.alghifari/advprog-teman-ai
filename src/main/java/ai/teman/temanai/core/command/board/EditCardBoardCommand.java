package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.*;
import com.linecorp.bot.model.event.source.Source;

public class EditCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/editCard";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length == 4) {
            String listName = messageLst[1];
            String oldCardName = messageLst[2];
            String newCardName = messageLst[3];

            try {
                TrelloCard card = TrelloApi.getInstance().getCard(boardId, listName, oldCardName);
                card.name = newCardName;
                TrelloApi.getInstance().updateCard(card.id, card);
                return "Card " + oldCardName + " is successfully updated";
            } catch (ListNotFoundException e) {
                return "List " + listName + " is not found";
            } catch (CardNotFoundException e) {
                return "Card " + oldCardName + " is not found";
            }
        } else {
            return "Invalid argument for /editCard. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "";
    }

}
