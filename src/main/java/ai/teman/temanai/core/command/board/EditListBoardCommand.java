package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.*;
import com.linecorp.bot.model.event.source.Source;

public class EditListBoardCommand implements BoardCommand {
    @Override
    public String getName() {
        return "/editList";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageList = message.split("\n");
        if (messageList.length == 3) {
            String oldListName = messageList[1];
            String newListName = messageList[2];
            try {
                TrelloList list = TrelloApi.getInstance().getList(boardId, oldListName);
                list.name = newListName;
                TrelloApi.getInstance().updateList(list.id, list);
                return "List " + oldListName + " is successfully updated";
            } catch (ListNotFoundException e) {
                return "List " + oldListName + " is not found";
            }
        } else {
            return "Invalid argument for /editList. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "Rename a Trello List\n"
                + "How to use:\n/editList\nOldName\nNewName";
    }

}
