package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloCard;
import ai.teman.temanai.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;

public class MoveCardBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/move";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        String[] messageLst = message.split("\n");
        if (messageLst.length != 4) {
            return "Invalid argument for /move. See /help for details.";
        }

        String oldListName = messageLst[1];
        String newListName = messageLst[3];
        String cardName = messageLst[2];

        if (oldListName.equals(newListName)) {
            return "Error: Old list and new list have the same name.";
        }

        TrelloList[] lists = TrelloApi.getInstance().getBoardLists(boardId);

        TrelloList oldList = null;
        TrelloList newList = null;
        for (TrelloList list : lists) {
            if (list.name.equals(oldListName)) {
                oldList = list;
            }

            if (list.name.equals(newListName)) {
                newList = list;
            }
        }

        if (oldList == null) {
            return String.format("List '%s' not found on connected board.", oldListName);
        }

        if (newList == null) {
            return String.format("List '%s' not found on connected board.", newListName);
        }

        TrelloCard[] cards = TrelloApi.getInstance().getListCards(oldList.id);

        for (TrelloCard card : cards) {
            if (card.name.equals(cardName)) {
                card.idList = newList.id;

                TrelloApi.getInstance().updateCard(card.id, card);
                return String.format("Card '%s' on list '%s' is successfully moved to list '%s'.",
                        card.name, oldList.name, newList.name);
            }
        }

        return String.format("Card '%s' is not found on list '%s'.",
                cardName, oldList.name);
    }

    @Override
    public String getDetail() {
        return "Command untuk memindahkan card di suatu list ke list lain.\n"
                + "How to use:\n/move\namaListLama\nnamaCard\nnamaListBaru\n"
                + "Kedua List harus ada di board, "
                + "dan namaListLama harus mempunyai card yang ingin dipindah";
    }
}
