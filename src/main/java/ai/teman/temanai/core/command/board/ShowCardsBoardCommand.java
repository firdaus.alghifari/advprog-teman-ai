package ai.teman.temanai.core.command.board;

import com.linecorp.bot.model.event.source.Source;

public class ShowCardsBoardCommand implements BoardCommand {

    @Override
    public String getName() {
        return "/showCards";
    }

    @Override
    public String processCommand(String message, Source chatSource, String boardId) {
        return "Simsalabim Abracadabra, muncul! "
                + "Tapi nanti ya, karena bot masih dalam development :)";
    }

    @Override
    public String getDetail() {
        return "Command untuk menampilkan semua Card dari List tertentu.\n"
                + "How to use:\n/showCards";
    }
}
