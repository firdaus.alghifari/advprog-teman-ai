package ai.teman.temanai.core.command.chatsource;

import ai.teman.temanai.core.command.base.BaseCommand;
import com.linecorp.bot.model.event.source.Source;

public interface ChatSourceCommand extends BaseCommand {
    String processCommand(String message, Source chatSource);
}
