package ai.teman.temanai.core.command.chatsource;

import ai.teman.temanai.data.BoardRoom;
import ai.teman.temanai.repository.BoardRoomRepo;
import com.linecorp.bot.model.event.source.Source;

public class ConnectBoardChatSourceCommand implements ChatSourceCommand {

    private BoardRoomRepo boardRoomRepo;

    public ConnectBoardChatSourceCommand(BoardRoomRepo boardRoomRepo) {
        this.boardRoomRepo = boardRoomRepo;
    }

    @Override
    public String getName() {
        return "/connectBoard";
    }

    @Override
    public String processCommand(String message, Source chatSource) {
        String[] messageLst = message.split(" ");
        if (messageLst.length == 2) {
            BoardRoom boardRoom = new BoardRoom(chatSource.getSenderId(), messageLst[1]);
            boardRoomRepo.save(boardRoom);
            return "Board " + messageLst[1] + " is successfully connected";
        } else {
            return "Invalid argument for /connectBoard. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "";
    }

}
