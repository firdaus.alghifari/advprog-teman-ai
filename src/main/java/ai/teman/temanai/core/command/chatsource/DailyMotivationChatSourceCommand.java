package ai.teman.temanai.core.command.chatsource;

import com.linecorp.bot.model.event.source.Source;

public class DailyMotivationChatSourceCommand implements ChatSourceCommand {
    @Override
    public String getName() {
        return "/setDailyMotivation";
    }

    @Override
    public String processCommand(String message, Source chatSource) {
        if (message.length() <= 19) {
            return "Belum bisa ya, karena kurang argumen jumlah hari. Silakan coba lagi atau ketik "
                    + "\"/help\""
                    + " untuk meminta bantuan";
        } else {
            return "Oke, motivasi akan diberikan dalam jangka waktu " + message.substring(20)
                    + " hari sekali "
                    + "dihitung mulai hari ini, tapi maaf belum bisa digunakan :)\n"
                    + "====================\n"
                    + "\"Be thankful for what you have, you'll end up having more\"";
        }

    }

    @Override
    public String getDetail() {
        return "Command untuk memberikan pesan motivasi.\n"
                + "How to use:\n/setDailyMotivation " + "jumlahHari\n"
                + "Contoh : /setDailyMotivation 3\n"
                + "Berarti motivasi akan diberikan dalam waktu 3 hari sekali.";
    }

}
