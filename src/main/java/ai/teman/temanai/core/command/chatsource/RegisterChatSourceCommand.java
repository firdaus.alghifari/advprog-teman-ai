package ai.teman.temanai.core.command.chatsource;

import ai.teman.temanai.data.Member;
import ai.teman.temanai.repository.MemberRepo;
import com.linecorp.bot.model.event.source.Source;

public class RegisterChatSourceCommand implements ChatSourceCommand {

    private MemberRepo memberRepo;

    public RegisterChatSourceCommand(MemberRepo memberRepo) {
        this.memberRepo = memberRepo;
    }

    @Override
    public String getName() {
        return "/register";
    }

    @Override
    public String processCommand(String message, Source chatSource) {
        String[] messageLst = message.split(" ");
        if (messageLst.length == 2) {
            Member member = new Member(chatSource.getUserId(), messageLst[1]);
            memberRepo.save(member);
            return "User " + messageLst[1] + " is successfully registered";
        } else {
            return "Invalid argument for /register. Read /help";
        }
    }

    @Override
    public String getDetail() {
        return "";
    }

}
