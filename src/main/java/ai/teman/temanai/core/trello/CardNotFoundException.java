package ai.teman.temanai.core.trello;

public class CardNotFoundException extends Exception {
    public CardNotFoundException() {

    }

    public CardNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
