package ai.teman.temanai.core.trello;

public class ListNotFoundException extends Exception {
    public ListNotFoundException() {

    }

    public ListNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
