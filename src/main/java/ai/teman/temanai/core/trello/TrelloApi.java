package ai.teman.temanai.core.trello;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

public class TrelloApi {
    private static TrelloApi instance;

    private RestTemplate restTemplate;

    private final String key = System.getenv("TRELLO_API_KEY");
    private final String token = System.getenv("TRELLO_API_TOKEN");

    private TrelloApi() {
        restTemplate = new RestTemplate();
    }

    public static TrelloApi getInstance() {
        if (instance == null) {
            instance = new TrelloApi();
        }
        return instance;
    }

    private String getSecrets() {
        return "?key=" + key + "&token=" + token;
    }

    public TrelloList[] getBoardLists(String boardId) {
        String uri = "https://api.trello.com/1/boards/" + boardId + "/lists" + getSecrets();
        return restTemplate.getForObject(uri, TrelloList[].class);
    }

    public TrelloCard[] getListCards(String listId) {
        String uri = "https://api.trello.com/1/lists/" + listId + "/cards" + getSecrets();
        return restTemplate.getForObject(uri, TrelloCard[].class);
    }

    public void updateCard(String cardId, TrelloCard updatedCard) {
        String uri = "https://api.trello.com/1/cards/" + cardId + getSecrets();
        restTemplate.put(uri, updatedCard);
    }

    public void createCard(TrelloCard newCard) {
        HttpEntity<TrelloCard> request = new HttpEntity<>(newCard);
        String uri = "https://api.trello.com/1/cards/" + getSecrets();
        restTemplate.postForObject(uri, request, TrelloCard.class);
    }

    public void createList(TrelloList newList) {
        HttpEntity<TrelloList> request = new HttpEntity<>(newList);
        String uri = "https://api.trello.com/1/lists/" + getSecrets();
        restTemplate.postForObject(uri, request, TrelloList.class);
    }

    public TrelloList getList (String boardId, String listName) throws ListNotFoundException {
        TrelloList[] trelloLists = getBoardLists(boardId);
        for (TrelloList list : trelloLists) {
            if (list.name.equalsIgnoreCase(listName)) {
                return list;
            }
        }
        throw new ListNotFoundException();
    }

    public void updateList(String listId, TrelloList updatedList) {
        String uri = "https://api.trello.com/1/lists/" + listId + getSecrets();
        restTemplate.put(uri, updatedList);
    }

    public TrelloCard getCard (String boardId, String listName, String cardName) throws
    ListNotFoundException, CardNotFoundException {
        TrelloList list = getList(boardId, listName);
        TrelloCard[] trelloCards = getListCards(list.id);
        for (TrelloCard card : trelloCards) {
            if (card.name.equalsIgnoreCase(cardName))
                return card;
        }
        throw new CardNotFoundException();
    }
}

