package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.basic.BasicCommand;
import org.springframework.stereotype.Repository;

@Repository
public class BasicCommandRepo extends BaseCommandRepo<BasicCommand> {
    public String processCommand(String cmdName, String message) {
        if (mapCommand.containsKey(cmdName)) {
            return mapCommand.get(cmdName).processCommand(message);
        } else {
            return null;
        }
    }
}
