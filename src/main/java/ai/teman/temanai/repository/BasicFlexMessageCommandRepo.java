package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.basic.BasicFlexMessageCommand;
import com.linecorp.bot.model.message.Message;
import org.springframework.stereotype.Repository;

@Repository
public class BasicFlexMessageCommandRepo extends BaseCommandRepo<BasicFlexMessageCommand> {
    public Message processCommand(String cmdName, String message) {
        if (mapCommand.containsKey(cmdName)) {
            return mapCommand.get(cmdName).processCommand(message);
        } else {
            return null;
        }
    }
}
