package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.board.BoardCommand;
import ai.teman.temanai.data.BoardRoom;
import com.linecorp.bot.model.event.source.Source;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class BoardCommandRepo extends BaseCommandRepo<BoardCommand> {

    private BoardRoomRepo boardRoom;

    @Autowired
    public BoardCommandRepo(BoardRoomRepo boardRoom) {
        super();
        this.boardRoom = boardRoom;
    }

    public String processCommand(String cmdName, String message, Source chatSource) {
        if (mapCommand.containsKey(cmdName)) {
            Optional<BoardRoom> optBoardRoom = boardRoom.findById(chatSource.getSenderId());
            if (optBoardRoom.isPresent()) {
                return mapCommand.get(cmdName).processCommand(message,
                        chatSource, optBoardRoom.get().getBoardId());
            } else {
                return "Board not found. Please /connectBoard using your Board ID";
            }
        } else {
            return null;
        }
    }
}
