package ai.teman.temanai.repository;

import ai.teman.temanai.data.BoardRoom;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BoardRoomRepo extends JpaRepository<BoardRoom, String> {

}
