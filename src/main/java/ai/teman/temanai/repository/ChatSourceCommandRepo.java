package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.chatsource.ChatSourceCommand;
import com.linecorp.bot.model.event.source.Source;
import org.springframework.stereotype.Repository;

@Repository
public class ChatSourceCommandRepo extends BaseCommandRepo<ChatSourceCommand> {
    /**
     * .
     * @param cmdName
     * @param message
     * @param chatSource
     * @return
     */
    public String processCommand(String cmdName, String message, Source chatSource) {
        if (mapCommand.containsKey(cmdName)) {
            return mapCommand.get(cmdName).processCommand(message, chatSource);
        } else {
            return null;
        }
    }
}
