package ai.teman.temanai.repository;

import ai.teman.temanai.data.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepo extends JpaRepository<Member, String> {

}
