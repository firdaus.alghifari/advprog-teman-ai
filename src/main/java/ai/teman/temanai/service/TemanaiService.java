package ai.teman.temanai.service;

import com.linecorp.bot.model.event.source.Source;

public interface TemanaiService {
    void replyMessage(String replyToken, String message, Source chatSource);
}
