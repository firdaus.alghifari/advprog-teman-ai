package ai.teman.temanai.service;

import ai.teman.temanai.core.command.basic.*;
import ai.teman.temanai.core.command.board.*;
import ai.teman.temanai.core.command.chatsource.ConnectBoardChatSourceCommand;
import ai.teman.temanai.core.command.chatsource.DailyMotivationChatSourceCommand;
import ai.teman.temanai.core.command.chatsource.RegisterChatSourceCommand;
import ai.teman.temanai.repository.*;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.source.Source;
import com.linecorp.bot.model.message.Message;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.model.response.BotApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

@Service
public class TemanaiServiceImpl implements TemanaiService {

    private static final Logger LOGGER = Logger.getLogger(TemanaiServiceImpl.class.getName());
    private LineMessagingClient lineMessagingClient;

    private BasicCommandRepo basicCommandRepo;
    private BasicFlexMessageCommandRepo basicFlexMessageCommandRepo;
    private ChatSourceCommandRepo chatSourceCommandRepo;
    private BoardCommandRepo boardCommandRepo;

    private BoardRoomRepo boardRoom;
    private MemberRepo memberRepo;

    @Autowired
    public TemanaiServiceImpl(
            LineMessagingClient lineMessagingClient,
            BasicCommandRepo basicCommandRepo,
            BasicFlexMessageCommandRepo basicFlexMessageCommandRepo,
            ChatSourceCommandRepo chatSourceCommandRepo,
            BoardCommandRepo boardCommandRepo,
            BoardRoomRepo boardRoom,
            MemberRepo memberRepo
    ) {
        this.lineMessagingClient = lineMessagingClient;
        this.basicCommandRepo = basicCommandRepo;
        this.basicFlexMessageCommandRepo = basicFlexMessageCommandRepo;
        this.chatSourceCommandRepo = chatSourceCommandRepo;
        this.boardCommandRepo = boardCommandRepo;
        this.boardRoom = boardRoom;
        this.memberRepo = memberRepo;
        seed();
    }

    private void seed() {
        // Register to basicCommandRepo if your Command only needs the message
        basicCommandRepo.registerCommand(new KerangAjaibBasicCommand());
        basicCommandRepo.registerCommand(new FunFactBasicCommand());

        // Register to basicFlexMessageCommandRepo
        basicFlexMessageCommandRepo.registerCommand(new HelpBasicCommand());

        /* Register to chatSourceCommandRepo if your Command needs the message
         and the sender information */
        chatSourceCommandRepo.registerCommand(new RegisterChatSourceCommand(memberRepo));
        chatSourceCommandRepo.registerCommand(new ConnectBoardChatSourceCommand(boardRoom));
        chatSourceCommandRepo.registerCommand(new DailyMotivationChatSourceCommand());

        // Register to boardCommandRepo if your Command needs the message and the board id
        boardCommandRepo.registerCommand(new EditCardBoardCommand());
        boardCommandRepo.registerCommand(new ArchiveCardBoardCommand());
        boardCommandRepo.registerCommand(new MoveCardBoardCommand());
        boardCommandRepo.registerCommand(new CreateCardBoardCommand());
        boardCommandRepo.registerCommand(new EditListBoardCommand());
        boardCommandRepo.registerCommand(new CompleteBoardCommand());
        boardCommandRepo.registerCommand(new CreateListBoardCommand());
        boardCommandRepo.registerCommand(new ShowCardsBoardCommand());
    }

    @Override
    public void replyMessage(String replyToken, String message, Source chatSource) {
        String[] messageLst = message.split("[ \\n]");
        String result = basicCommandRepo.processCommand(messageLst[0], message);
        if (result == null) {
            result = chatSourceCommandRepo.processCommand(messageLst[0], message, chatSource);
        }
        if (result == null) {
            result = boardCommandRepo.processCommand(messageLst[0], message, chatSource);
        }
        if (result == null && messageLst[0].equals("/help")){
            Message helpFlexMessage = basicFlexMessageCommandRepo.processCommand(messageLst[0], message);
            sendReplyMessage(replyToken, helpFlexMessage);
        }
        if (result != null) {
            TextMessage textMessage = new TextMessage(result);
            sendReplyMessage(replyToken, textMessage);
        } else if (message.length() > 1 && message.charAt(0) == '/') {
            TextMessage textMessage = new TextMessage("Command anda gak ketemu");
            sendReplyMessage(replyToken, textMessage);
        }
    }

    private void sendReplyMessage(String replyToken, Message toSend) {
        try {
            CompletableFuture<BotApiResponse> response = lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, toSend));
            if (response != null) {
                response.get();
            }

        } catch (InterruptedException | ExecutionException e) {
            LOGGER.warning("Ada error saat ingin membalas chat");
            Thread.currentThread().interrupt();
        }
    }
}
