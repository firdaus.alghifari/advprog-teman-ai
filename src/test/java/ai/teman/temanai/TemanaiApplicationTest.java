package ai.teman.temanai;

import ai.teman.temanai.service.TemanaiService;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TemanaiApplicationTest {

    @InjectMocks
    TemanaiApplication temanaiApplication;

    @Mock
    TemanaiService temanaiService;

    @BeforeEach
    void setUp() {
        temanaiService = mock(TemanaiService.class);
        temanaiApplication = new TemanaiApplication(temanaiService);
    }

    @Test
    void testReplyMessage() {
        Source source = mock(Source.class);
        TextMessageContent textMessageContent = new TextMessageContent("tmc1", "hello");
        MessageEvent<TextMessageContent> messageEvent = new MessageEvent<TextMessageContent>(
                "a", source, textMessageContent, null
        );
        temanaiApplication.handleTextEvent(messageEvent);
        verify(temanaiService, times(1)).replyMessage(
                "a", "hello", source
        );
    }

}
