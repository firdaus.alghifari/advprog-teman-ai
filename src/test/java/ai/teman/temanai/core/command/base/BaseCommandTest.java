package ai.teman.temanai.core.command.base;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BaseCommandTest {

    private Class<?> commandClass;

    @BeforeEach
    void setUp() {
        commandClass = BaseCommand.class;
    }

    @Test
    void testCommandIsAPublicInterface() {
        int classModifiers = commandClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    void testCommandHasGetNameAbstractMethod() throws Exception {
        Method cast = commandClass.getDeclaredMethod("getName");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }

    @Test
    void testCommandHasGetDetailAbstractMethod() throws Exception {
        Method cast = commandClass.getDeclaredMethod("getDetail");
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(0, cast.getParameterCount());
    }
}
