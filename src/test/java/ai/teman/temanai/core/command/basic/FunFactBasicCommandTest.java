package ai.teman.temanai.core.command.basic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class FunFactBasicCommandTest {
    private FunFactBasicCommand funFactCommand;

    @BeforeEach
    public void setUp() {
        funFactCommand = new FunFactBasicCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/funFact", funFactCommand.getName());
    }

    @Test
    public void testProcessCommand(){
        String funFact = funFactCommand.processCommand("");

        assertTrue(funFactCommand.getFunFactList().contains(funFact));
    }

    @Test
    public void testGetDetail(){
        assertEquals("Sebuah command yang akan memberikan sebuah fun fact :D.\n" +
                "How to use:\n/funFact",
                funFactCommand.getDetail());
    }
}
