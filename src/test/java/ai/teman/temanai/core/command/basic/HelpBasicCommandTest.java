package ai.teman.temanai.core.command.basic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HelpBasicCommandTest {
    private HelpBasicCommand helpCommand;

    @BeforeEach
    public void setUp() {
        helpCommand = new HelpBasicCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/help", helpCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk menampilkan bantuan dalam menggunakan Bot Temanai.\n" +
                        "How to use:\n/help",
                helpCommand.getDetail());
    }
}

