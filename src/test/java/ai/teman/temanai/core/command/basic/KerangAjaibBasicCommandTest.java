package ai.teman.temanai.core.command.basic;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class KerangAjaibBasicCommandTest {
    private KerangAjaibBasicCommand kerangAjaib;

    @BeforeEach
    void setUp() {
        kerangAjaib = new KerangAjaibBasicCommand();
    }

    @Test
    void testGetName() {
        assertEquals("/apakah", kerangAjaib.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(kerangAjaib.getDetail());
    }

    @Test
    void testProcessCommand() {
        Set<String> outputPossibility = new HashSet<>();
        outputPossibility.add("Ya");
        outputPossibility.add("Engga");

        for (int i = 0; i < 10; ++i)
            assertTrue(outputPossibility.contains(kerangAjaib.processCommand("saya hebat?")));
    }
}
