package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.CardNotFoundException;
import ai.teman.temanai.core.trello.ListNotFoundException;
import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloCard;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;

@ExtendWith(MockitoExtension.class)
class ArchiveCardBoardCommandTest {

    @InjectMocks
    private ArchiveCardBoardCommand archiveCard;

    @Mock
    private TrelloApi trelloApi;

    @BeforeEach
    void setUp() {
        archiveCard = new ArchiveCardBoardCommand();
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void testGetName() {
        assertEquals("/archiveCard", archiveCard.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(archiveCard.getDetail());
    }

    @Test
    void testProcessCommandFail() {
        String result = archiveCard.processCommand("test", null, "");
        assertTrue(result != null && !result.equals(""));
    }

    @Test
    void testProcessCommandSuccess() {
        setMock(trelloApi);
        TrelloCard card = mock(TrelloCard.class);
        card.name = "B";
        card.id = "meong";

        try {
            when(trelloApi.getCard("board1", "A", "B"))
                    .thenReturn(card);
        } catch (Exception ignored) {}
        doNothing().when(trelloApi).updateCard("meong", card);

        Source source = mock(Source.class);

        String result = archiveCard.processCommand("/archiveCard\nA\nB",
                source, "board1");
        assertEquals("Card B is successfully archived", result);

        try {
            when(trelloApi.getCard("board2", "A", "B"))
                    .thenThrow(new ListNotFoundException());
        } catch (Exception ignored) {}

        result = archiveCard.processCommand("/archiveCard\nA\nB",
                source, "board2");
        assertEquals("List A is not found", result);

        try {
            when(trelloApi.getCard("board3", "A", "B"))
                    .thenThrow(new CardNotFoundException());
        } catch (Exception ignored) {}

        result = archiveCard.processCommand("/archiveCard\nA\nB",
                source, "board3");
        assertEquals("Card B is not found", result);
    }
}
