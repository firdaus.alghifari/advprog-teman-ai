package ai.teman.temanai.core.command.board;

import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BoardCommandTest {
    private Class<?> commandClass;

    @BeforeEach
    void setUp() {
        commandClass = BoardCommand.class;
    }

    @Test
    void testCommandIsAPublicInterface() {
        int classModifiers = commandClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    void testCommandHasProcessCommandAbstractMethod() throws Exception {
        Method cast = commandClass.getDeclaredMethod("processCommand", String.class, Source.class, String.class);
        int methodModifiers = cast.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals(3, cast.getParameterCount());
    }
}
