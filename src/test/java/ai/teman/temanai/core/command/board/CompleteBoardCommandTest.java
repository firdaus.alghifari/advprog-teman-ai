package ai.teman.temanai.core.command.board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CompleteBoardCommandTest {
    private CompleteBoardCommand completeCommand;

    @BeforeEach
    public void setUp() {
        completeCommand = new CompleteBoardCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/complete", completeCommand.getName());
    }

    @Test
    public void testProcessCommand(){
        String result = completeCommand.processCommand(completeCommand.getName(), null, "");
        assertTrue(result != null && result != "");
    }

    @Test
    public void testGetDetail() {
        assertEquals("Change task status in Trello Card that have due date to 'complete'\n" +
                        "How to use:\n/complete\nListName\nCardName",
                completeCommand.getDetail());
    }
}


