package ai.teman.temanai.core.command.board;

import ai.teman.temanai.core.trello.TrelloApi;
import ai.teman.temanai.core.trello.TrelloList;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CreateListBoardCommandTest {
    @Mock
    private TrelloApi trelloApi;

    @InjectMocks
    private CreateListBoardCommand createListCommand;

    @Test
    public void testGetName() {
        assertEquals("/createList", createListCommand.getName());
    }

    @Test
    public void testGetDetail() {
        assertEquals("Command untuk membuat List.\n" +
                "How to use:\n/createList\nnamaList", createListCommand.getDetail());
    }

    //Setup method to mock Singleton class
    //Reference: https://stackoverflow.com/questions/38914433/mocking-a-singleton-with-mockito
    private void setMock(TrelloApi mock) {
        try {
            Field instance = TrelloApi.class.getDeclaredField("instance");
            instance.setAccessible(true);
            instance.set(instance, mock);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testProcessCommand() {
        //Init mocks
        setMock(trelloApi);
        TrelloList trelloList = mock(TrelloList.class);
        trelloList.name = "ToDoList";
        trelloList.idBoard = "boardTest";

        Source source = mock(Source.class);

        //Test invalid argument
        assertEquals("Tidak ada List yang dibuat. Silakan coba lagi atau ketik " + "\"/help\"" + " untuk meminta bantuan",
                createListCommand.processCommand("/createList", source, "boardTest"));

        //Test valid argument
        assertEquals("List 'ToDoList' berhasil dibuat",
                createListCommand.processCommand("/createList\nToDoList", source, "boardTest"));
        verify(trelloApi).createList(any(TrelloList.class));
    }

}
