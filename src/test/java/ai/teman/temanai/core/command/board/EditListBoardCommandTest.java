package ai.teman.temanai.core.command.board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EditListBoardCommandTest {
    private EditListBoardCommand editListCommand;

    @BeforeEach
    public void setUp() {
        editListCommand = new EditListBoardCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/editList", editListCommand.getName());
    }

    @Test
    public void testProcessCommand(){
        String result = editListCommand.processCommand(editListCommand.getName(), null, "");
        assertTrue(result != null && result != "");
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Rename a Trello List\n"
                        + "How to use:\n/editList\nOldName\nNewName",
                editListCommand.getDetail());
    }
}

