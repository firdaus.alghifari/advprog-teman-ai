package ai.teman.temanai.core.command.board;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShowCardsBoardCommandTest {
    private ShowCardsBoardCommand showCards;

    @BeforeEach
    public void setUp() {
        showCards = new ShowCardsBoardCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/showCards", showCards.getName());
    }

    @Test
    public void testProcessCommand(){
        assertEquals("Simsalabim Abracadabra, muncul! Tapi nanti ya, karena bot masih dalam development :)",
                showCards.processCommand("/showCards", null, ""));
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk menampilkan semua Card dari List tertentu.\n" +
                "How to use:\n/showCards", showCards.getDetail());
    }
}
