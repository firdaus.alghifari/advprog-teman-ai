package ai.teman.temanai.core.command.chatsource;

import ai.teman.temanai.data.BoardRoom;
import ai.teman.temanai.repository.BoardRoomRepo;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConnectBoardChatSourceCommandTest {

    @InjectMocks
    private ConnectBoardChatSourceCommand connectBoard;

    @Mock
    private BoardRoomRepo boardRoomRepo;

    @Mock
    Source chatSource;

    @BeforeEach
    void setUp() {
        BoardRoomRepo boardRoom = mock(BoardRoomRepo.class);
        connectBoard = new ConnectBoardChatSourceCommand(boardRoom);
    }

    @Test
    void testGetName() {
        assertEquals("/connectBoard", connectBoard.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(connectBoard.getDetail());
    }

    @Test
    void testProcessCommandFail() {
        String result = connectBoard.processCommand("test", null);
        assertTrue(result != null && !result.equals(""));
    }

    @Test
    void testProcessCommandSuccess() {
        chatSource = mock(Source.class);
        boardRoomRepo = mock(BoardRoomRepo.class);

        String result = connectBoard.processCommand("/connectBoard board1",
                chatSource);
        assertEquals("Board board1 is successfully connected", result);
    }

}
