package ai.teman.temanai.core.command.chatsource;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DailyMotivationChatSourceCommandTest {
    private DailyMotivationChatSourceCommand dailyMotivation;

    @BeforeEach
    public void setUp() {
        dailyMotivation = new DailyMotivationChatSourceCommand();
    }

    @Test
    public void testGetName() {
        assertEquals("/setDailyMotivation", dailyMotivation.getName());
    }

    @Test
    public void testProcessCommand(){
        String argument = "2";
        assertEquals("Belum bisa ya, karena kurang argumen jumlah hari. Silakan coba lagi atau ketik " +
                "\"/help\"" + " untuk meminta bantuan", dailyMotivation.processCommand("/setDailyMotivation",
                null) );

        assertEquals("Oke, motivasi akan diberikan dalam jangka waktu " + argument + " hari sekali " +
                        "dihitung mulai hari ini, tapi maaf belum bisa digunakan :)\n" +
                        "====================\n" +
                        "\"Be thankful for what you have, you'll end up having more\""
                , dailyMotivation.processCommand("/setDailyMotivation " + argument, null) );
    }

    @Test
    public void testGetDetail() {
        assertEquals( "Command untuk memberikan pesan motivasi.\n"
                + "How to use:\n/setDailyMotivation " + "jumlahHari\n"
                + "Contoh : /setDailyMotivation 3\n"
                + "Berarti motivasi akan diberikan dalam waktu 3 hari sekali.",
                dailyMotivation.getDetail());
    }
}
