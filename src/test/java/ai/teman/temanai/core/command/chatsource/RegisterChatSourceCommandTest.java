package ai.teman.temanai.core.command.chatsource;

import ai.teman.temanai.data.Member;
import ai.teman.temanai.repository.MemberRepo;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RegisterChatSourceCommandTest {

    @InjectMocks
    private RegisterChatSourceCommand registerChat;

    @Mock
    private MemberRepo memberRepo;

    @Mock
    Source chatSource;

    @BeforeEach
    void setUp() {
        MemberRepo boardRoom = mock(MemberRepo.class);
        registerChat = new RegisterChatSourceCommand(boardRoom);
    }

    @Test
    void testGetName() {
        assertEquals("/register", registerChat.getName());
    }

    @Test
    void testGetDetail() {
        assertNotNull(registerChat.getDetail());
    }

    @Test
    void testProcessCommandFail() {
        String result = registerChat.processCommand("test", null);
        assertTrue(result != null && !result.equals(""));
    }

    @Test
    void testProcessCommandSuccess() {
        chatSource = mock(Source.class);
        memberRepo = mock(MemberRepo.class);

        String result = registerChat.processCommand("/register username",
                chatSource);
        assertEquals("User username is successfully registered", result);
    }
}
