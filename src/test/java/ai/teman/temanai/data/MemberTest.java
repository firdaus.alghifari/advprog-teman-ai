package ai.teman.temanai.data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MemberTest {

    private Member member;

    @BeforeEach
    void setUp() throws Exception {
        member = new Member("cat1", "the_cat");
    }

    @Test
    void testEmptyConstructor() {
        member = new Member();
        assertNotNull(member);
    }

    @Test
    void testGetId() {
        assertEquals("cat1", member.getId());
    }

    @Test
    void testGetUsername() {
        assertEquals("the_cat", member.getUsername());
    }

    @Test
    void testSetId() {
        member.setId("gukguk");
        assertEquals("gukguk", member.getId());
    }

    @Test
    void testSetUsername() {
        member.setUsername("super_cat");
        assertEquals("super_cat", member.getUsername());
    }

}
