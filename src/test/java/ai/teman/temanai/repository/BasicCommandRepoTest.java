package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.basic.BasicCommand;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BasicCommandRepoTest {
    public BasicCommandRepo basicCommandRepo;

    @BeforeEach
    public void setUp(){
        basicCommandRepo = new BasicCommandRepo();
    }

    @Test
    public void testProcessCommandShouldReturnResultOfProcessCommandIfCommandExist(){
        BasicCommand mockCommand = mock(BasicCommand.class);
        when(mockCommand.getName()).thenReturn("/mock");
        when(mockCommand.processCommand(anyString())).thenReturn("Mocked Result");
        basicCommandRepo.registerCommand(mockCommand);

        assertEquals("Mocked Result",
                basicCommandRepo.processCommand("/mock", "/mock"));
    }

    @Test
    public void testProcessCommandShouldReturnNullIfCommandDoesNotExist(){
        assertNull(basicCommandRepo.processCommand("/doesnt-exist", "/null"));
    }
}
