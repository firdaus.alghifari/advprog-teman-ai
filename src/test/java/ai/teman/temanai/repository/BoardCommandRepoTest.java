package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.board.BoardCommand;
import ai.teman.temanai.data.BoardRoom;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BoardCommandRepoTest {
    public BoardCommandRepo boardCommandRepo;

    @Mock
    private Source sourceMock;

    @Mock
    private Source sourceMockFalse;

    @Mock
    private BoardRoomRepo boardRoomRepoMock;


    @Test
    public void testProcessCommand(){
        when(sourceMock.getSenderId()).thenReturn("idtest");
        when(sourceMockFalse.getSenderId()).thenReturn("idfalse");

        BoardRoom boardRoomMock = mock(BoardRoom.class);
        when(boardRoomMock.getBoardId()).thenReturn("idtest");
        when(boardRoomRepoMock.findById("idtest")).thenReturn(Optional.of(boardRoomMock));
        when(boardRoomRepoMock.findById("idfalse")).thenReturn(Optional.empty());

        boardCommandRepo = new BoardCommandRepo(boardRoomRepoMock);

        BoardCommand mockCommand = mock(BoardCommand.class);
        when(mockCommand.getName()).thenReturn("/mock");
        when(mockCommand.processCommand(anyString(), any(Source.class), anyString())).
                thenReturn("Mocked Result");

        boardCommandRepo.registerCommand(mockCommand);

        //Command exist and Board exist
        assertEquals("Mocked Result",
                boardCommandRepo.processCommand("/mock", "/mock", sourceMock));

        //Command exist but Board does not exist
        assertEquals("Board not found. Please /connectBoard using your Board ID",
                boardCommandRepo.processCommand("/mock", "/mock", sourceMockFalse));

        //Command does not exist
        assertNull(boardCommandRepo.processCommand(
                "/doesnt-exist", "/null", sourceMock));
    }
}
