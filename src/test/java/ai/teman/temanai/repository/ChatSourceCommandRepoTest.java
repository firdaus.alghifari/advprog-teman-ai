package ai.teman.temanai.repository;

import ai.teman.temanai.core.command.chatsource.ChatSourceCommand;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ChatSourceCommandRepoTest {
    public ChatSourceCommandRepo chatSourceCommandRepo;

    @Mock
    private Source sourceMock;

    @BeforeEach
    public void setUp(){
        chatSourceCommandRepo = new ChatSourceCommandRepo();
    }

    @Test
    public void testProcessCommandShouldReturnResultOfProcessCommandIfCommandExist(){
        ChatSourceCommand mockCommand = mock(ChatSourceCommand.class);
        when(mockCommand.getName()).thenReturn("/mock");
        when(mockCommand.processCommand(anyString(), any(Source.class))).thenReturn("Mocked Result");
        chatSourceCommandRepo.registerCommand(mockCommand);

        assertEquals("Mocked Result",
                chatSourceCommandRepo.processCommand("/mock", "/mock", sourceMock));
    }

    @Test
    public void testProcessCommandShouldReturnNullIfCommandDoesNotExist(){
        assertNull(chatSourceCommandRepo.processCommand(
                "/doesnt-exist", "/null", sourceMock));
    }
}
