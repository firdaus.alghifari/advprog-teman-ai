package ai.teman.temanai.service;

import ai.teman.temanai.core.command.basic.FunFactBasicCommand;
import ai.teman.temanai.core.command.basic.HelpBasicCommand;
import ai.teman.temanai.core.command.basic.KerangAjaibBasicCommand;
import ai.teman.temanai.core.command.board.*;
import ai.teman.temanai.core.command.chatsource.ConnectBoardChatSourceCommand;
import ai.teman.temanai.core.command.chatsource.DailyMotivationChatSourceCommand;
import ai.teman.temanai.core.command.chatsource.RegisterChatSourceCommand;
import ai.teman.temanai.repository.*;
import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.source.Source;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TemanaiServiceImplTest {

    @Mock
    private LineMessagingClient lineMessagingClient;

    @Mock
    private BasicCommandRepo basicCommandRepo;

    @Mock
    private BasicFlexMessageCommandRepo basicFlexMessageCommandRepo;

    @Mock
    private ChatSourceCommandRepo chatSourceCommandRepo;

    @Mock
    private BoardCommandRepo boardCommandRepo;

    @Mock
    private BoardRoomRepo boardRoomRepo;

    @Mock
    private MemberRepo memberRepo;


    @InjectMocks
    private TemanaiServiceImpl service;

    private TemanaiServiceImpl makeService(){
        return new TemanaiServiceImpl(
                lineMessagingClient,
                basicCommandRepo,
                basicFlexMessageCommandRepo,
                chatSourceCommandRepo,
                boardCommandRepo,
                boardRoomRepo,
                memberRepo
        );
    }

    @Test
    public void whenNewServiceIsCreatedItShouldCallSeedAndRegisterAllCommand(){
        makeService();

        verify(basicCommandRepo, atLeastOnce()).registerCommand(any(FunFactBasicCommand.class));
        verify(basicCommandRepo, atLeastOnce()).registerCommand(any(KerangAjaibBasicCommand.class));

        verify(basicFlexMessageCommandRepo, atLeastOnce()).registerCommand(any(HelpBasicCommand.class));

        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CreateCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(ArchiveCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(EditCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(MoveCardBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CreateListBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(ShowCardsBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(CompleteBoardCommand.class));
        verify(boardCommandRepo, atLeastOnce()).registerCommand(any(EditCardBoardCommand.class));

        verify(chatSourceCommandRepo, atLeastOnce()).registerCommand(any(ConnectBoardChatSourceCommand.class));
        verify(chatSourceCommandRepo, atLeastOnce()).registerCommand(any(RegisterChatSourceCommand.class));
        verify(chatSourceCommandRepo, atLeastOnce()).registerCommand(any(DailyMotivationChatSourceCommand.class));
    }

    @Test
    public void whenReplyMessageIsCalledItShouldCallProcessCommandAndCallLineClientReplyMessage() throws ExecutionException, InterruptedException {
        //Mock source
        Source source = mock(Source.class);

        //Tes ketika message tidak diawali '/' maka tidak mereply
        service.replyMessage("token","test abc", source);
        verify(basicCommandRepo).processCommand("test", "test abc");
        verify(boardCommandRepo).processCommand("test", "test abc", source);
        verify(chatSourceCommandRepo).processCommand("test", "test abc", source);
        verify(lineMessagingClient, times(0)).replyMessage(any(ReplyMessage.class));

        //Tes ketika message diawali '/' maka mereply
        service.replyMessage("token","/test abc", source);

        verify(basicCommandRepo).processCommand("/test", "/test abc");
        verify(boardCommandRepo).processCommand("/test", "/test abc", source);
        verify(chatSourceCommandRepo).processCommand("/test", "/test abc", source);
        verify(lineMessagingClient).replyMessage(any(ReplyMessage.class));

        //Tes ketika message diawali '/' dan commandnya valid maka mereply dengan response
        when(basicCommandRepo.processCommand("/funFact", "/funFact a")).
                thenReturn("Sebuah Fun fact");

        CompletableFuture response = mock(CompletableFuture.class);
        when(lineMessagingClient.replyMessage(any(ReplyMessage.class))).thenReturn(response);

        service.replyMessage("token", "/funFact a", source);

        verify(basicCommandRepo).processCommand("/funFact", "/funFact a");
        verify(lineMessagingClient, times(2)).replyMessage(any(ReplyMessage.class));
        verify(response).get();

        //Tes ketika message diawali '/' dan commandnya valid maka mereturn sesuatu
        when(response.get()).thenThrow(new InterruptedException());

        service.replyMessage("token", "/error-command", source);
        verify(lineMessagingClient, times(3)).replyMessage(any(ReplyMessage.class));
        verify(response, times(2)).get();
    }
}
